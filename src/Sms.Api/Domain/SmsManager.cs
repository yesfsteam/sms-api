﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SD.Messaging.Models;
using Sms.Api.Data;
using Yes.Infrastructure.Common.Extensions;
using Yes.Sms.Api.Contracts.Commands;

namespace Sms.Api.Domain
{
    public interface ISmsManager
    {
        Task<ProcessingResult> SendSms(SendSmsCommand command);
    }
    
    public class SmsManager : ISmsManager
    {
        private readonly ILogger<SmsManager> logger;
        private readonly ISmsGatewayClient client;
        private readonly IRepository repository;

        public SmsManager(ILogger<SmsManager> logger, ISmsGatewayClient client, IRepository repository)
        {
            this.logger = logger;
            this.client = client;
            this.repository = repository;
        }

        public async Task<ProcessingResult> SendSms(SendSmsCommand command)
        {
            var beginTime = DateTime.Now;
            try
            {
                await repository.SaveSms(command);
                
                var response = await client.SendSms(command.PnoneNumber, command.Message);
                if (!response.IsSuccessStatusCode)
                {
                    logger.LogWarning($"Failed to send SMS. Duration: {beginTime.GetDuration()} Request: {command}, Response: {response.ErrorMessage}");
                    return ProcessingResult.Fail();
                }
                    
                logger.LogInformation($"SMS successfully sent. Duration: {beginTime.GetDuration()} {command}");
                return ProcessingResult.Ok();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while sending SMS. Duration: {beginTime.GetDuration()} Request: {command}");
                return ProcessingResult.Ok();
            }
        }
        
    }
}
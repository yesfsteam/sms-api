﻿﻿ using System;

  namespace Sms.Api.Models.Configuration
{
    public class SmsGatewayConfiguration
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Action { get; set; }
        public string Sender { get; set; }
        public TimeSpan Timeout { get; set; }
    }
}
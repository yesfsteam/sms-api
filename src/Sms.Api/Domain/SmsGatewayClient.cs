﻿using System.Net.Http;
using System.Threading.Tasks;
using Sms.Api.Models.Configuration;
using Yes.Infrastructure.Http;

namespace Sms.Api.Domain
{
    public interface ISmsGatewayClient
    {
        Task<Response> SendSms(string phoneNumber, string message);
    }
    
    public class SmsGatewayClient : RestClientBase, ISmsGatewayClient
    {
        private readonly SmsGatewayConfiguration configuration;

        public SmsGatewayClient(HttpClient client, SmsGatewayConfiguration configuration) : base(client, configuration.Host)
        {
            client.Timeout = configuration.Timeout;
            this.configuration = configuration;
        }

        public async Task<Response> SendSms(string phoneNumber, string message)
        {
            return await Get(string.Empty, new
            {
                user = configuration.Username,
                pass = configuration.Password,
                action = configuration.Action,
                sender = configuration.Sender,
                target = phoneNumber,
                message
            });
        }
    }
}
using System.IO;
using System.Net.Http;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using SD.Cqrs;
using SD.Cqrs.NetCore;
using SD.Logger.Serilog.NetCore;
using SD.Messaging.RabbitMQ.NetCore;
using Serilog;
using Sms.Api.Data;
using Sms.Api.Domain;
using Sms.Api.Extensions;
using Sms.Api.Messaging;
using Sms.Api.Models.Configuration;
using Yes.Contracts;
using Yes.Sms.Api.Contracts.Commands;

namespace Sms.Api
{
    public class Startup
    {
	    private readonly string applicationName;
	    
	    public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            applicationName = Assembly.GetExecutingAssembly().GetName().Name;
        }

        private readonly IConfiguration configuration;
        
        public void ConfigureServices(IServiceCollection services)
        {
	        var messagingConfiguration = configuration.BindFromAppConfig<MessagingConfiguration>();
	        services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddCqrsEngine()
	            .WithSerilogLogger()
	            .WithRabbitMQMessagingEngine()
	            .WithBoundedContext(BoundedContext.Create(BoundedContexts.SMS_API)
		            .ListeningCommands(typeof(SendSmsCommand))
		            .Build())
	            .AddCommandHandler<SmsCommandHandler>(messagingConfiguration.ChannelsCount);
            
            services.AddTransient<ISmsManager, SmsManager>();
            services.AddTransient<IRepository, Repository>();
            services.AddTransient<ISmsGatewayClient, SmsGatewayClient>();
            services.AddSingleton(configuration.BindFromAppConfig<SmsGatewayConfiguration>());
            services.AddTransient<HttpClient>();

            if (configuration.GetValue<bool>("EnableSwagger"))
	            services.AddSwaggerGen(c =>
	            {
		            c.SwaggerDoc("v1", new OpenApiInfo { Title = applicationName, Version = "v1" });
		            c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, $"{applicationName}.xml"));
	            });
           
			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

            DapperInitializer.ConfigureDapper();
		}

        public void Configure(IApplicationBuilder app, IHostApplicationLifetime applicationLifetime, CqrsEngine cqrsEngine)
        {
	        applicationLifetime.ApplicationStopping.Register(()=>OnApplicationStopping(cqrsEngine));

	        app.UseRouting();
	        app.UseEndpoints(endpoints => endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"));

	        if (configuration.GetValue<bool>("EnableSwagger"))
	        {
		        app.UseSwagger();
		        app.UseSwaggerUI(c =>
		        {
			        c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{applicationName} v1");
			        c.RoutePrefix = string.Empty;
		        });
	        }
            
	        cqrsEngine.Start();
            
	        Log.Logger.Information($"{applicationName} has been started");
        }

        private void OnApplicationStopping(CqrsEngine cqrsEngine)
        {
	        cqrsEngine.Stop();
	        Log.Logger.Information($"{applicationName} has been stopped");
        }
	}
}
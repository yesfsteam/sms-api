﻿using System;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Yes.Sms.Api.Contracts.Commands;

namespace Sms.Api.Data
{
    public interface IRepository
    {
        Task SaveSms(SendSmsCommand command);
    }

    public class Repository : IRepository
    {
        private readonly IConfiguration configuration;

        public Repository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveSms(SendSmsCommand command)
        {
            var queryParams = new
            {
                command.PnoneNumber,
                command.Message,
                CreatedDate = DateTime.Now
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO sms_history (
                    phone_number,
                    message,
                    created_date
                ) VALUES (
                    :pnoneNumber,
                    :message,
                    :createdDate
                );", queryParams);
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}
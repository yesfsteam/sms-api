﻿using System.Threading.Tasks;
using SD.Cqrs;
using SD.Messaging.Models;
using Sms.Api.Domain;
using Yes.Sms.Api.Contracts.Commands;

namespace Sms.Api.Messaging
{
    public class SmsCommandHandler : CommandHandler
    {
        private readonly ISmsManager manager;

        public SmsCommandHandler(ISmsManager manager) 
        {
            this.manager = manager; 
        }

        public async Task<ProcessingResult> Handle(SendSmsCommand command)
            => await manager.SendSms(command);
    }
}